News
====

1.4
---
*Release date: 2024-09-03*

* Misc non code updates
* ruff auto formatting
* Fixed errors and warnings reported by ruff and mypy

1.3
---
*Release date: 2020-12-16*

* Removed support for Python 2.x
* Switched from setup.py to poetry

1.2
---
*Release date: 2016-08-26*

* Removed a double normalization

1.1
---
*Release date: 2014-07-01*

* added items() == iteritems() to storage for Py3 style compat.

1.0.2
-----

* More doctests, almost full coverage.
* Fixed bug relating to normalization
* get_doc now returns a denormalized copy of the internal dict

1.0.1
-----

*Release date: 2014-06-18*

* include the code in the package...

1.0
---

*Release date: 2014-06-18*

 storage was previusly a part of hcs_utils (hcs_utils.storage)

