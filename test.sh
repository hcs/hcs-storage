#!/bin/sh

exec poetry run pytest --doctest-modules storage.py "$@"
