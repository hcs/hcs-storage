About
=====

a storage class wrapping a dict.


Feedback and getting involved
-----------------------------

Send feedback and bug reports by email to hcs at furuvik dot net.

- Code Repository: https://gitlab.com/hcs/hcs-storage
